﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using MstLibrary.Wpf.DataBinding;
using MstLibrary.Data;
using System.Runtime.Serialization;

namespace MstDisplay
{
    class PictureViewModel
    {
        public PictureModel Data { get; private set; }
        public BindCommand Command { get; private set; }

        public PictureViewModel()
        {
            Data = new PictureModel();
            Command = new BindCommand()
            {
                ExecuteHandler = Execute,
                CanExecuteHandler = CanExecute
            };
            Data.Initialize();
            Data.ConnectServer();
        }

        private void Execute(object parameter)
        {
            Data.ImageSource = @"C:\Users\yuma\Downloads\display\images\11.png";
            Data.MediaStates[0] = MediaState.Stop;
            Data.MediaStates[1] = MediaState.Stop;
            Data.BgmSource = new Uri(@"C:\Users\yuma\Desktop\MSTBGM集\序章・旅立ち・道中\miyako_japan2.mp3", UriKind.Absolute);
            Data.VoiceSource = new Uri(@"C:\Users\yuma\Downloads\MP3\6_2.mp3", UriKind.Absolute);
            Data.MediaStates[0] = MediaState.Play;
            Data.MediaStates[1] = MediaState.Play;
        }

        private bool CanExecute(object parameter)
        {
            return true;
        }
    }

}
