﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MstLibrary.Wpf.DataBinding;
using System.IO;
using MstLibrary.Network;
using System.IO.Compression;
using MstLibrary.Data;
using System.Runtime.Serialization;
using WebSocket4Net;
using Codeplex.Data;
using System.Media;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using Ionic.Zip;
using Ionic.Zlib;
using System.Diagnostics;

namespace MstDisplay
{
    public class PictureModel : NotificationObject
    {
        private const string ImagesPath = "books";
        private string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
        private string imagePass;
        private string id;
        private string url;
        private Uri bgmPath;
        private Uri voicePath;
        private Uri[] sePath = new Uri[10];
        private MediaState[] state = new MediaState[10];
        private WebSocket socket;
        private int currentVoice;
        private int page;

        public string ImageSource
        {
            get { return imagePass; }
            set
            {
                SetProperty<string>(ref imagePass, value);
                Debug.WriteLine(value);
            }
        }

        private Uri Address
        {
            get
            {
                foreach (var line in File.ReadLines("settings.ini"))
                {
                    string[] keyValue = line.Split('=');
                    if (keyValue[0].Trim().ToLower() == "server")
                        return new Uri(keyValue[1].Trim());
                }

                return null;
            }
        }

        public string BookId
        {
            get { return id; }
            set { SetProperty<string>(ref id, value); }
        }

        public string BookUrl
        {
            get { return url; }
            set { SetProperty<string>(ref url, value); }
        }

        public Uri BgmSource
        {
            get { return bgmPath; }
            set { SetProperty<Uri>(ref bgmPath, value); }
        }

        public Uri[] SeSource
        {
            get { return sePath; }
            set { SetProperty<Uri[]>(ref sePath, value); }
        }

        public Uri VoiceSource
        {
            get { return voicePath; }
            set { SetProperty<Uri>(ref voicePath, value); }
        }

        public MediaState[] MediaStates
        {
            get { return state; }
            set { SetProperty<MediaState[]>(ref state, value); }
        }

        public void Initialize()
        {
            if (!Directory.Exists(ImagesPath))
                Directory.CreateDirectory(ImagesPath);
        }

        public async Task fetchBookAsync(string id)
        {
            // TODO IDを受け取るMethodを作る
            BookId = id;
            string DirectoryPath = Path.Combine(stCurrentDir, "books", BookId);

            string[] paths = {
                                 "",
                                 "images",
                                 "bgm",
                                 "se",
                                 "voice"
                             };

            // TODO 絵本IDに対応するDirectoryをimagesフォルダの中に作る

            foreach (var path in paths)
            {
                string dir = Path.Combine(DirectoryPath, path);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
            }
            // TODO URLからZIPを落とす
            using (RestClient client = new RestClient())
            using (Stream stream = await client.GetStreamAsync("http://" + Address + "/mst/api/v1/book/" + BookId + "/display"))
            using (FileStream file = new FileStream(Path.Combine(DirectoryPath, BookId + ".zip"), FileMode.Create))
            {
                await stream.CopyToAsync(file);
            }
            // TODO ZIPを絵本Directoryの中に解凍
            string unzipPath = Path.GetFullPath(DirectoryPath);
            string fullZipPath = Path.GetFullPath(DirectoryPath + @"\" + BookId + ".zip");
            var options = new ReadOptions
            {
                //UTF8にエンコード
                Encoding = System.Text.Encoding.UTF8
            };
            using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(fullZipPath, options))
            {
                lock (zip)
                {
                    //すべてのファイルを展開する
                    zip.ExtractAll(unzipPath, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                }
            };
        }

        public void fetchPageAsync(int page)
        {
            //ページ同期したら音声を止める
            MediaStates[1] = MediaState.Stop;
            //イメージソースを切り替える
            if (File.Exists(Path.Combine(stCurrentDir, @"books\" + BookId + @"\images\" + page + ".png")))
                ImageSource = Path.Combine(stCurrentDir, @"books\" + BookId + @"\images\" + page + ".png");
        }

        public async Task playingVoice(int no)
        {
            if (File.Exists(Path.Combine(stCurrentDir, @"books\" + BookId + @"\voice\" + page + "_" + no + ".mp3")))
            {
                MediaStates[1] = MediaState.Stop;
                VoiceSource = new Uri(Path.Combine(stCurrentDir, @"books\" + BookId + @"\voice\" + page + "_" + no + ".mp3"));
                MediaStates[1] = MediaState.Play;
            }
        }

        //サーバーに接続
        public void ConnectServer()
        {
            socket = new WebSocket("ws://" + Address + ":9000");
            socket.Opened += (object sender, EventArgs e) =>
            {
                socket.Send("{\"jsonrpc\" : \"2.0\", \"method\" : \"handshake\", \"params\" : [\"user01\", { \"kind\" : \"display\" }], \"id\" : 1 }");
            };
            socket.MessageReceived += OnMessage;
            socket.Open();
        }


        //Json受け取る
        private async void OnMessage(object sender, MessageReceivedEventArgs e)
        {
            dynamic response = DynamicJson.Parse(e.Message);

            Debug.WriteLine("JSON:" + e.Message);

            if (response.result == null)
                return;
            int messageId = (int)response.result.messageId;

            switch (messageId)
            {
                case 1: // ハンドShake
                    socket.Send("{\"jsonrpc\" : \"2.0\", \"method\" : \"takeOpenedBook\", \"params\" : [], \"id\" : 1 }");
                    break;
                case 2: // ページ同期
                    page = (int)response.result.page;
                    fetchPageAsync(page);
                    break;
                case 3: //本を開く
                    BookId = response.result.bookId;
                    //await fetchBookAsync(BookId);
                    fetchPageAsync(0);
                    break;
                case 4: //ボイスの再生
                    currentVoice = (int)response.result.no;
                    await playingVoice(currentVoice);
                    break;
            }
        }

        public async void OnVoiceEnded()
        {
            currentVoice++;
            if (File.Exists(Path.Combine(stCurrentDir, @"books\" + BookId + @"\voice\" + page + "_" + currentVoice + ".mp3")) /* if: 次の音声の存在チェック */)
            {
                // true: あれば再生
                await playingVoice(currentVoice);
            }
            else
            {
                // false: なければメッセージ送信
                socket.Send("{\"jsonrpc\" : \"2.0\", \"method\" : \"relayvoice\", \"params\" : [" + currentVoice + ", \"app\" ], \"id\" : 1 }");
            }
        }
    }
}
