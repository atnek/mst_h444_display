﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MstDisplay
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<MediaElement> mediaList;
        private PictureViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = viewModel = new PictureViewModel();
        }
        
        private void createMediaElement()
        {
            mediaList = new List<MediaElement>();
            var element = new MediaElement();
            element.Source = new Uri("");

            this.AddChild(element);
        }

        private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {   
            var player = (MediaElement)sender;
            player.Position = TimeSpan.Zero;
            player.Play();
        }

        private void MediaElement_MediaEnded_1(object sender, RoutedEventArgs e)
        {
            viewModel.Data.OnVoiceEnded();
        }


        
    }
}
