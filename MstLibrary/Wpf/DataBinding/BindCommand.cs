﻿using System;
using System.Windows.Input;

namespace MstLibrary.Wpf.DataBinding
{
    public class BindCommand : ICommand
    {
        public Action<object> ExecuteHandler { get; set; }
        public Func<object, bool> CanExecuteHandler { get; set; }

        #region ICommand メンバー

        public bool CanExecute(object parameter)
        {
            return CanExecuteHandler == null ? true : CanExecuteHandler(parameter);
        }

        public event EventHandler CanExecuteChanged;

        public BindCommand()
        {
            CanExecuteHandler = (parameter) => true;
        }

        public void Execute(object parameter)
        {
            if (ExecuteHandler != null)
                ExecuteHandler(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged(this, null);
        }

        #endregion
    }
}
