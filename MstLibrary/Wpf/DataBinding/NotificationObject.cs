﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Windows.Threading;

namespace MstLibrary.Wpf.DataBinding
{
    public class NotificationObject : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        private Dictionary<string, bool> haserrors = new Dictionary<string, bool>();

        public event PropertyChangedEventHandler PropertyChanged;

        public Dispatcher Dispatcher { get; set; }

        protected void OnPropertyChanged([CallerMemberName] string propertyname = null)
        {
            if (PropertyChanged != null)
            {
                if (Dispatcher != null)
                    Dispatcher.Invoke(() => PropertyChanged(this, new PropertyChangedEventArgs(propertyname)));
                else
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }

        protected bool SetProperty<T>(ref T store, T value, [CallerMemberName] string propertyname = null)
        {
            if (Equals(store, value))
                return false;

            store = value;
            OnPropertyChanged(propertyname);
            return true;
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        private void OnErrorsChanged([CallerMemberName] string propertyname = null)
        {
            if (ErrorsChanged != null)
            {
                if (Dispatcher != null)
                    Dispatcher.Invoke(() => ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyname)));
                else
                    ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyname));
            }
        }

        public bool Validate(Func<bool> checkmethod, [CallerMemberName] string propertyname = null)
        {
            bool result = checkmethod();

            if (!result)
                OnErrorsChanged(propertyname);

            return result;
        }

        public IEnumerable GetErrors(string propertyName)
        {
            //TODO 実装
            throw new System.NotImplementedException();
        }

        public bool HasErrors
        {
            get
            {
                foreach (KeyValuePair<string, bool> e in haserrors)
                {
                    if (e.Value)
                        return true;
                }

                return false;
            }
        }
    }
}
