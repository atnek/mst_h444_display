﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Net;
using System.Net.Http.Headers;

namespace MstLibrary.Network
{
    public class RestClient : IDisposable
    {
        private HttpClient client;

        public Uri BaseUri { get; set; }

        public AuthenticationHeaderValue Auth { get; private set; }

        public RestClient()
        {
            client = new HttpClient();
        }

        public RestClient(string baseUri)
            : this()
        {
            BaseUri = new Uri(baseUri);
        }

        public RestClient(Uri baseUri)
            : this()
        {
            BaseUri = baseUri;
        }

        public void addAuth(string user, string password)
        {
            var authData = string.Format("{0}:{1}", user, password);
            var binary = Encoding.UTF8.GetBytes(authData);
            Auth = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(binary));
            client.DefaultRequestHeaders.Authorization = Auth;
        }

        public void DeleteAuth()
        {
            if (client.DefaultRequestHeaders.Authorization != null)
                client.DefaultRequestHeaders.Authorization = null;
        }

        public async Task<string> GetStringAsync(string uri)
        {
            string message;
            Uri url = BaseUri != null ? new Uri(BaseUri, uri) : new Uri(uri);
            using (HttpResponseMessage res = await client.GetAsync(url))
                message = await res.Content.ReadAsStringAsync();

            return message;
        }

        public async Task<Stream> GetStreamAsync(string uri)
        {
            Uri url = BaseUri != null ? new Uri(BaseUri, uri) : new Uri(uri);
            return await (await client.GetAsync(url)).Content.ReadAsStreamAsync();
        }

        public async Task<string> PostStringAsync(string uri, Dictionary<string, string> contents)
        {
            var postData = new FormUrlEncodedContent(contents);
            string message;
            using (HttpResponseMessage res = await client.PostAsync(new Uri(BaseUri, uri), postData))
                message = await res.Content.ReadAsStringAsync();

            return message;
        }

        public async Task<string> PutStringAsync(string uri, Dictionary<string, string> contents)
        {
            var postData = new FormUrlEncodedContent(contents);
            string message;
            using (HttpResponseMessage res = await client.PutAsync(new Uri(BaseUri, uri), postData))
                message = await res.Content.ReadAsStringAsync();

            return message;
        }

        public async Task<string> DeleteStringAsync(string uri)
        {
            string message;
            using (HttpResponseMessage res = await client.DeleteAsync(new Uri(BaseUri, uri)))
                message = await res.Content.ReadAsStringAsync();

            return message;
        }

        public void Dispose()
        {
            client.Dispose();
            client = null;
        }
    }
}
