﻿using System.IO;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

namespace MstLibrary.Data
{
    public static class JsonObjectMapper
    {
        /// <summary>
        /// インスタンスをJSONに変換
        /// </summary>
        /// <typeparam name="T">インスタンスの型</typeparam>
        /// <param name="instance">インスタンス</param>
        /// <returns>JSON</returns>
        public static string Write<T>(T instance)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));

            string result = "";

            using (MemoryStream stream = new MemoryStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                serializer.WriteObject(stream, instance);
                stream.Position = 0;
                result = reader.ReadToEnd();
            }

            return result;
        }

        /// <summary>
        /// JSONをインスタンスに変換
        /// </summary>
        /// <typeparam name="T">インスタンスの型</typeparam>
        /// <param name="json">JSON</param>
        /// <returns>インスタンス</returns>
        public static T Read<T>(string json)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));

            T result = default(T);

            using (MemoryStream stream = new MemoryStream())
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.Write(json);
                writer.Flush();
                stream.Position = 0;
                result = (T)serializer.ReadObject(stream);
            }

            return result;
        }

        #region 非同期

        /// <summary>
        /// 非同期でインスタンスをJSONに変換
        /// </summary>
        /// <typeparam name="T">インスタンスの型</typeparam>
        /// <param name="instance">インスタンス</param>
        /// <returns>JSON</returns>
        public static async Task<string> WriteAsync<T>(T instance)
        {
            return await Task.Run<string>(() => Write<T>(instance));
        }

        /// <summary>
        /// 非同期でJSONをインスタンスに変換
        /// </summary>
        /// <typeparam name="T">インスタンスの型</typeparam>
        /// <param name="json">JSON</param>
        /// <returns>インスタンス</returns>
        public static async Task<T> ReadAsync<T>(string json)
        {
            return await Task.Run<T>(() => Read<T>(json));
        }

        #endregion
    }
}
