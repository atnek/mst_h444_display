﻿using System.Text;

namespace MstLibrary.Text
{
    public class UTF8 : CharacterCode
    {
        internal override string Encode(byte[] binary)
        {
            return Encoding.UTF8.GetString(binary);
        }

        internal override byte[] Decode(string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }
    }
}
