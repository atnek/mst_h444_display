﻿
namespace MstLibrary.Text
{
    public abstract class CharacterCode
    {
        internal abstract string Encode(byte[] binary);
        internal abstract byte[] Decode(string str);
    }
}
