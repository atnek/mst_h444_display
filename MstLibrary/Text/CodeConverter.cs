﻿
namespace MstLibrary.Text
{
    public class CodeConverter
    {
        public CharacterCode CharacterCode { get; set; }

        public CodeConverter(CharacterCode code)
        {
            CharacterCode = code;
        }

        public string Encode(byte[] binary)
        {
            return CharacterCode.Encode(binary);
        }

        public byte[] Decode(string str)
        {
            return CharacterCode.Decode(str);
        }
    }
}
