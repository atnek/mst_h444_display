﻿using System;

namespace MstLibrary.Text
{
    public class Base64 : CharacterCode
    {
        internal override string Encode(byte[] binary)
        {
            return Convert.ToBase64String(binary);
        }

        internal override byte[] Decode(string base64String)
        {
            return Convert.FromBase64String(base64String);
        }
    }
}
